import axios from "axios";

const service = axios.create({
    responseType: 'arraybuffer'
})

//响应拦截器
service.interceptors.response.use(resp => {
    const headers = resp.headers;
    let reg = RegExp(/application\/json/);
    if (headers['content-type'].match(reg)) {
        resp.data = unitToString(resp.data);
    } else {
        let file = require('js-file-download');
        let fileName = headers['content-disposition'].split(';')[1].split('filename=')[1];
        let contentType = headers['content-type'];
        fileName = decodeURIComponent(fileName);
        file(resp.data, fileName, contentType);
    }

}, error => {
    console.log(error);
})

function unitToString(unitArr) {
    let encodeStr = String.fromCharCode.apply(null, new Uint8Array(unitArr));
    let decodeStr = decodeURIComponent(escape(encodeStr));
    return JSON.parse(decodeStr);
}

let base = '/toy';
export const downloadRequest = (url, params) => {
    return service({
        method: 'get',
        url: `${base}${url}`,
        data: params
    });
}

export default service;
