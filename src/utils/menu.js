import {getRequest} from "./api";

export const initMenu = (router, store) => {
    if (store.state.routes.length > 0) {
        return;
    }

    getRequest('/menu/list').then(response => {
        if (response) {
            //格式化Router
            let fmtRoutes = formatRoutes(response.data);
            //添加到路由
            router.addRoutes(fmtRoutes);
            //将数据存入vuex中
            store.commit('initRoutes', fmtRoutes);
            //连接websocket
            store.dispatch('connect');
        }
    })
}

export const formatRoutes = (routes) => {
    let fmtRoutes = [];
    routes.forEach(router => {
        let {
            path,
            component,
            name,
            iconClass,
            children
        } = router;

        if (children && children instanceof Array) {
            //递归
            children = formatRoutes(children);
        }

        let fmRouter = {
            path: path,
            name: name,
            iconClass: iconClass,
            children: children,
            component(resolve) {
                if (component.startsWith('Home')) {
                    require(['../views/' + component + '.vue'], resolve);
                } else if (component.startsWith('User')) {
                    require(['../views/user/' + component + '.vue'], resolve);
                } else if (component.startsWith('Store')) {
                    require(['../views/store/' + component + '.vue'], resolve);
                } else if (component.startsWith('Product')) {
                    require(['../views/product/' + component + '.vue'], resolve);
                } else if (component.startsWith('Posting')) {
                    require(['../views/posting/' + component + '.vue'], resolve);
                } else if (component.startsWith('Order')) {
                    require(['../views/order/' + component + '.vue'], resolve);
                } else if (component.startsWith('Match')) {
                    require(['../views/match/' + component + '.vue'], resolve);
                } else if (component.startsWith('Category')) {
                    require(['../views/category/' + component + '.vue'], resolve);
                } else if (component.startsWith('Brand')) {
                    require(['../views/brand/' + component + '.vue'], resolve);
                } else if (component.startsWith('Echarts')) {
                    require(['../views/echarts/' + component + '.vue'], resolve);
                }
            }
        }
        fmtRoutes.push(fmRouter);
    });
    return fmtRoutes;
}
