import axios from 'axios'
import {Message} from 'element-ui'
import router from "../router/router";

//响应拦截器
axios.interceptors.response.use(success => {
    let status = success.status;
    //业务逻辑
    if (status && status === 200) {
        //接口返回的code
        let code = success.data.code;
        //接口返回的message
        let message = success.data.message;
        //业务逻辑出错
        if (code === 500 || code === 403 || code === 401 || code === 2005 ||
            code === 3008 || code === 5555) {
            Message.error({message: message, center: true});
            return;
        }
        if (code === 2008 || code === 2009 /*|| code === 2007*/) {
            Message.error({message: message, center: true});
            router.push('/');
        }
    }
    return success.data;
}, error => {
    let errorCode = error.response.status;
    console.log(errorCode + JSON.stringify(error.response.data))
    if (errorCode === 504 || errorCode === 404) {
        Message.error({message: '服务器出错，访问失败！', center: true});
    } else if (errorCode === 403) {
        Message.error({message: '权限不足，请联系管理员', center: true});
    } else if (errorCode === 401) {
        Message.error({message: '尚未登录，请登录', center: true});
        router.replace('/');
    } else if (errorCode === 500) {
        Message.error({message: '服务器错误，请稍后重试！', center: true});
    } else {
        if (error.response.data.message) {
            Message.error({message: error.response.data.message, center: true});
        } else {
            Message.error({message: '未知错误！', center: true});
        }
    }
    return;
});

let baseURL = '/toy';

export const postRequest = (url, params) => {
    return axios({
        method: 'post',
        url: `${baseURL}${url}`,
        data: params
    })
}
export const getRequest = (url, params) => {
    return axios({
        method: 'get',
        url: `${baseURL}${url}`,
        data: params
    })
}

export const putRequest = (url, params) => {
    return axios({
        method: 'put',
        url: `${baseURL}${url}`,
        data: params
    })
}

export const patchRequest = (url, params) => {
    return axios({
        method: 'patch',
        url: `${baseURL}${url}`,
        data: params
    })
}

export const deleteRequest = (url, params) => {
    return axios({
        method: 'delete',
        url: `${baseURL}${url}`,
        data: params
    })
}
