import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import './assets/iconfont/iconfont.css'
import 'area-linkage-vue/dist/index.css';
import {postRequest} from "@/utils/api";
import {getRequest} from "@/utils/api";
import {patchRequest} from "@/utils/api";
import {putRequest} from "@/utils/api";
import {deleteRequest} from "@/utils/api";
import {initMenu} from "./utils/menu";
import {downloadRequest} from "@/utils/download";
import {pcaa} from 'area-data-vue';
import store from "@/store/store";
import router from './router/router'
import AreaLinkageVue from 'area-linkage-vue';
import qs from 'qs';

Vue.use(ElementUI)
Vue.use(AreaLinkageVue)
Vue.config.productionTip = false

Vue.prototype.pcaa = pcaa;
Vue.prototype.qs = qs;
Vue.prototype.postRequest = postRequest;
Vue.prototype.getRequest = getRequest;
Vue.prototype.patchRequest = patchRequest;
Vue.prototype.putRequest = putRequest;
Vue.prototype.deleteRequest = deleteRequest;
Vue.prototype.downloadRequest = downloadRequest;


//全局导航守卫 (路由跳转时加载菜单)
router.beforeEach((to, from, next) => {
    //判断用户是否登录
    if (window.sessionStorage.getItem('user')) {
        initMenu(router, store);
        next();
    } else {
        //如果用户未登录访问内部页面则跳至登录页面
        if (to.path === '/') {
            next();
        } else {
            next('/?redirect=' + to.path);
        }
    }
})

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app')

