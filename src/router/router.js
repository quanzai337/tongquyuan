import Vue from 'vue'
import VueRouter from "vue-router"
import Home from "@/views/Home";
import ModifyPassword from "@/components/ModifyPassword";
import Login from "@/views/Login";
import FriendChat from "@/views/chat/FriendChat";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Login',
        component: Login,
        hidden: true,
    },
    {
        path: '/home',
        name: 'Home',
        component: Home,
        children: [
            {
                path: '/modifyPassword',
                name: 'ModifyPassword',
                component: ModifyPassword,
                hidden: true,
            },
            {
                path: "/chat",
                name: "在线聊天",
                component: FriendChat
            },
        ]
    },
]

const router = new VueRouter({
    routes
})

export default router