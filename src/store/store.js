import Vue from 'vue'
import Vuex from 'vuex'
import SockJS from 'sockjs-client'
import Stomp from 'stompjs'
import {getRequest} from "@/utils/api";
import {Notification} from 'element-ui';

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        routes: [],
        //聊天列表
        sessions: {},
        //好友列表
        admins: [],
        currentAdmin: JSON.parse(window.sessionStorage.getItem('user')),
        currentSession: null,
        filterKey: '',
        stomp: null,
        idDot: {}
    },

    mutations: {
        initRoutes(state, data) {
            state.routes = data;
        },

        init_admin(state, admin) {
            state.currentAdmin = admin;
        },

        //改变当前聊天对象
        changeCurrentSession(state, currentSession) {
            state.currentSession = currentSession;
            Vue.set(state.idDot, state.currentAdmin.username
                + '#' + state.currentSession.rootName, false);
        },

        //发送信息
        addMessage(state, msg) {
            let mss = state.sessions[state.currentAdmin.rootName + '#' + msg.toName];
            if (!mss) {
                // state.sessions[state.currentAdmin.rootName + '#' + msg.toName] = [];
                Vue.set(state.sessions, state.currentAdmin.rootName + '#' + msg.toName, [])
            }
            state.sessions[state.currentAdmin.rootName + '#' + msg.toName].push({
                content: msg.content,
                date: new Date(),
                self: !msg.notSelf
            })
        },
        INIT_DATA(state) {
            //浏览器本地的历史聊天记录
            let data = localStorage.getItem('vue-chat-session');
            if (data) {
                state.sessions = JSON.parse(data);
            }
        },
        //初始化好友列表
        INIT_ADMINS(state, data) {
            state.admins = data;
        }
    },
    actions: {
        //连接WebSocket
        connect(context) {
            context.state.stomp = Stomp.over(new SockJS('/toy/ws/ep'));
            context.state.stomp.connect({}, success => {
                console.log("连接成功" + success);
                //连接到聊天频道
                context.state.stomp.subscribe('/user/queue/chat', msg => {
                    //消息接收
                    let receiveMessage = JSON.parse(msg.body);
                    if (context.state.currentSession.rootName !== receiveMessage.fromName
                        || !context.state.currentSession) {
                        Notification.success({
                            title: '【' + receiveMessage.fromName + '】发来消息',
                            message: receiveMessage.content.length > 10
                                ? receiveMessage.content.substring(0, 10) : receiveMessage.content,
                            position: 'bottom-right',
                        });
                        Vue.set(context.state.idDot, context.state.currentAdmin.username
                            + '#' + receiveMessage.fromName, true);
                    }
                    receiveMessage.notSelf = true;
                    receiveMessage.toName = receiveMessage.fromName;
                    context.commit('addMessage', receiveMessage);
                })
            }, error => {
                console.log("连接失败" + error);
            })
        },
        //初始化聊天信息记录
        initData(context) {
            context.commit('INIT_DATA');
            //获取好友列表
            getRequest('/root/friendList').then(resp => {
                if (resp.code === 200) {
                    context.commit('INIT_ADMINS', resp.data)
                }
            })
        }
    }
})
store.watch(function (state) {
    return state.sessions
}, function (val) {
    localStorage.setItem('vue-chat-session', JSON.stringify(val));
}, {
    deep: true/*这个貌似是开启watch监测的判断,官方说明也比较模糊*/
})

export default store;