let proxyObj = {}

proxyObj['/toy'] = {
    //websocket
    ws: false,
    //目标地址
    target: 'http://localhost:8080',
    //发送请求头host会被设置target
    changeOrigin: true,
    //不重写请求地址
    pathReWrite: {
        '^/toy': '/'
    }
}

proxyObj['/ws'] = {
    ws: true,
    target: 'ws://localhost:8080',
};

module.exports = {
    publicPath: './',
    devServer: {
       /* host: 'localhost',*/
        port: 8081,
        proxy: proxyObj
    },
}




